package com.bedo.commands;

import java.util.ArrayList;
import java.util.List;

import com.bedo.mod.BlockFillerPositionSelector;

import net.minecraft.block.Block;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class BlockFillCommand implements ICommand {

	private List aliases = new ArrayList();
	private Block block;

	public BlockFillCommand() {
		aliases.add("fillblocks");
		aliases.add("fb");
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		// TODO Auto-generated method stub
		return "fillblocks <block ID>";
	}

	@Override
	public List getAliases() {
		// TODO Auto-generated method stub
		return aliases;
	}

	private void sendErrorMessage(ICommandSender sender, String message) {
		sender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED
				+ message));
	}

	private void swapPositions(int index) {
		int temp = BlockFillerPositionSelector.pos1.get(index);
		BlockFillerPositionSelector.pos1.set(index,
				BlockFillerPositionSelector.pos2.get(index));
		BlockFillerPositionSelector.pos2.set(index, temp);
	}

	@Override
	public void execute(ICommandSender sender, String[] args)
			throws CommandException {
		// TODO Auto-generated method stub
		if (args.length != 1) {
			sendErrorMessage(sender, "Invalid number of arguments!");
			return;
		}
		try {
			block = Block.getBlockById(Integer.parseInt(args[0]));
			if (block == Blocks.air && Integer.parseInt(args[0]) != 0) {
				sendErrorMessage(sender, "The argument \"" + args[0]
						+ "\" is not a valid block ID!");
				return;
			}
		} catch (NumberFormatException e) {
			if (Block.getBlockFromName(args[0]) == null) {
				sendErrorMessage(sender, "The argument \"" + args[0]
						+ "\" is not a valid block name!");
				return;
			} else {
				block = Block.getBlockFromName(args[0]);
			}
		}

		if (BlockFillerPositionSelector.pos1.isEmpty()
				|| BlockFillerPositionSelector.pos2.isEmpty()) {
			sendErrorMessage(sender,
					"E5tar el awel el positions, balash 3'abawaa");
			return;
		}

		if (BlockFillerPositionSelector.pos1.get(0) > BlockFillerPositionSelector.pos2
				.get(0)) {
			swapPositions(0);
		}
		if (BlockFillerPositionSelector.pos1.get(1) > BlockFillerPositionSelector.pos2
				.get(1)) {
			swapPositions(1);
		}
		if (BlockFillerPositionSelector.pos1.get(2) > BlockFillerPositionSelector.pos2
				.get(2)) {
			swapPositions(2);
		}
		EntityPlayer player = (EntityPlayer) sender;
		
		/*for (int x = BlockFillerPositionSelector.pos1.get(0); x <= BlockFillerPositionSelector.pos2
				.get(0); x++) {
			for (int y = BlockFillerPositionSelector.pos1.get(1); y <= BlockFillerPositionSelector.pos2
					.get(1); y++) {
				for (int z = BlockFillerPositionSelector.pos1.get(2); z <= BlockFillerPositionSelector.pos2
						.get(2); z++) {
					player.worldObj.setBlockState(
							new BlockPos(x, y, z), block.getBlockState()
									.getBaseState());
				}
			}
		}*/
		for (int x = BlockFillerPositionSelector.pos1.get(0); x <= BlockFillerPositionSelector.pos2
				.get(0); x++) {
			for (int y = BlockFillerPositionSelector.pos1.get(1); y <= BlockFillerPositionSelector.pos2
					.get(1); y++) {
				player.worldObj.setBlockState(
						new BlockPos(x, y, BlockFillerPositionSelector.pos1.get(2)), block.getBlockState()
								.getBaseState());
				player.worldObj.setBlockState(
						new BlockPos(x, y, BlockFillerPositionSelector.pos2.get(2)), block.getBlockState()
								.getBaseState());
			}
		}
		
		for (int x = BlockFillerPositionSelector.pos1.get(0); x <= BlockFillerPositionSelector.pos2
				.get(0); x++) {
			for (int z = BlockFillerPositionSelector.pos1.get(2); z <= BlockFillerPositionSelector.pos2
					.get(2); z++) {
				player.worldObj.setBlockState(
						new BlockPos(x, BlockFillerPositionSelector.pos1.get(1), z), block.getBlockState()
								.getBaseState());
				player.worldObj.setBlockState(
						new BlockPos(x, BlockFillerPositionSelector.pos2.get(1), z), block.getBlockState()
								.getBaseState());
			}
		}
		
		for (int y = BlockFillerPositionSelector.pos1.get(1); y <= BlockFillerPositionSelector.pos2
				.get(2); y++) {
			for (int z = BlockFillerPositionSelector.pos1.get(2); z <= BlockFillerPositionSelector.pos2
					.get(2); z++) {
				player.worldObj.setBlockState(
						new BlockPos(BlockFillerPositionSelector.pos1.get(0), y, z), block.getBlockState()
								.getBaseState());
				player.worldObj.setBlockState(
						new BlockPos(BlockFillerPositionSelector.pos2.get(0), y, z), block.getBlockState()
								.getBaseState());
			}
		}
		
		
		
		
		
		
	}


	
	@Override
	public boolean canCommandSenderUse(ICommandSender sender) {
		// TODO Auto-generated method stub
		return sender instanceof EntityPlayer;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender sender, String[] args,
			BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index) {
		// TODO Auto-generated method stub
		return false;
	}

}
