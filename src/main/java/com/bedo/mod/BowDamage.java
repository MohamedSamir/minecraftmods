package com.bedo.mod;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BowDamage {

	
	@SubscribeEvent
	public void arrowEvent(ArrowLooseEvent event){
		
		EntityArrow entityarrow = new EntityArrow(event.entityPlayer.worldObj, event.entityPlayer, (float) 2.0);
		entityarrow.setDamage(175);
		entityarrow.setFire(175);
		event.bow.damageItem(1, event.entityPlayer);
		int l = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, event.bow);

        if (l > 0)
        {
            entityarrow.setKnockbackStrength(l);
        }
        event.entityPlayer.worldObj.spawnEntityInWorld(entityarrow);
        event.entityPlayer.addChatMessage(new ChatComponentText(
				EnumChatFormatting.GREEN + "Edrab gamed gamed :D :D "));
	}
	
}
