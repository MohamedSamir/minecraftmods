package com.bedo.mod;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.entity.minecart.MinecartCollisionEvent;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ExplodingMinecarts {
	
	public int x;
	public int y;
	public int z;
	
	/*@SubscribeEvent
	public void sendMessage(BreakEvent event) {
		
	}*/
	
	@SubscribeEvent
	public void explode(PlayerInteractEvent  event){
		if(event.action == Action.RIGHT_CLICK_BLOCK ){
			x = event.pos.getX();
			y = event.pos.getY();
			z = event.pos.getZ();
			event.entityPlayer.addChatMessage(new ChatComponentText(
					EnumChatFormatting.GREEN + "Position  set to "
							+ x + ", " + y + ", "
							+ z + "."));
		}
		else {
			event.entityPlayer.worldObj.createExplosion(
					event.entityPlayer,
					x,
					y,
					z,
					2,
					true);
		}
		
		//Minecraft.getMinecraft().objectMouseOver
		/*int x = event.bow.();
		Entity object = Minecraft.getMinecraft().objectMouseOver.;
		BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
		event.entityPlayer.worldObj.createExplosion(
				object,
				pos.getX(),
				pos.getY(),
				pos.getZ(),
				2,
				true);*/
	}
	
	/*@SubscribeEvent
	public void explode(MinecartCollisionEvent event){
		EntityMinecart minecart = event.minecart;
		minecart.worldObj.createExplosion(
		minecart,
		minecart.posX,
		minecart.posY,
		minecart.posZ,
		2,
		false);
	}*/

}
