package com.bedo.mod;

import net.minecraft.block.BlockBed;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityWitch;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ItemsSpawn {
	@SubscribeEvent
	public void explode(BreakEvent event) {

		if (event.state.getBlock() == Blocks.cake) {
			EntityHorse horse = new EntityHorse(event.world);
			horse.setPosition(event.pos.getX(), event.pos.getY(), event.pos.getZ());
			event.getPlayer().worldObj.spawnEntityInWorld(horse);
			return;
		} else if(event.state.getBlock() == Blocks.stone) {
			EntityWitch witch = new EntityWitch(event.world);
			witch.setPosition(event.pos.getX(), event.pos.getY(), event.pos.getZ());
			
		}
		else if(event.state.getBlock() == Blocks.ice) {
			event.world.createExplosion(null, event.pos.getX(),
					event.pos.getY(), event.pos.getZ(), 10, true);
		}
	}

}
