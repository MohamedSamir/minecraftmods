package com.bedo.mod;

import com.bedo.commands.BlockFillCommand;
import com.bedo.commands.FlamingPigs;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Main.MODID, version = Main.VERSION)
public class Main {
	public static final String MODID = "myMods";
	public static final String VERSION = "1.0";

	public static Block enderBlock;

	@EventHandler
	public void registerCommands(FMLServerStartingEvent event) {
		event.registerServerCommand(new FlamingPigs());
		event.registerServerCommand(new BlockFillCommand());
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		
		MinecraftForge.EVENT_BUS.register(new BlockFillerPositionSelector());
		//MinecraftForge.EVENT_BUS.register(new BowDamage());
		//MinecraftForge.EVENT_BUS.register(new BlockBreakMessage());
		MinecraftForge.EVENT_BUS.register(new ItemsSpawn());
		//MinecraftForge.EVENT_BUS.register(new ExplodingMinecarts());
//		MinecraftForge.EVENT_BUS.register(new DiamondOreTrap());
//
//		MinecraftForge.EVENT_BUS.register(new BiggerTNTExplosion());
//		MinecraftForge.EVENT_BUS.register(new PigsDroppingDiamonds_2());
//		MinecraftForge.EVENT_BUS.register(new ZombieKnights());
//		MinecraftForge.EVENT_BUS.register(new CreeperReinforcements());
//
//		MinecraftForge.EVENT_BUS.register(new SuperJump());
//
//		FMLCommonHandler.instance().bus().register(new Parachute());
//		MinecraftForge.EVENT_BUS.register(new Parachute());
//
//		enderBlock = new EnderBlock();
//		GameRegistry.registerBlock(enderBlock, "enderBlock");
//
//		Item enderBlockItem = GameRegistry.findItem("mymods", "enderBlock");
//		ModelResourceLocation enderBlockModel = new ModelResourceLocation(
//				"mymods:enderBlock", "inventory");
//
//		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
//				.register(enderBlockItem, 0, enderBlockModel);

	}
}