package com.bedo.mod;

import java.util.Random;

import net.minecraft.entity.passive.EntityPig;
import net.minecraft.init.Items;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PigsDroppingDiamonds_2 {
	@SubscribeEvent
	public void dropDiamonds(LivingDeathEvent event) {
		if (!(event.entity instanceof EntityPig)) {
			return;
		}

		
		if (!event.entity.worldObj.isRemote) {
			int i=0;
			int amount = 2;
			while (i < 5)
			{
				event.entity.dropItem(Items.diamond, amount);
				i++;
			}
				
		}
	}

}
